# Frontend Mentor - QR code component solution

This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

## Overview

### Screenshot

![Desktop](https://qr-code-component-5kzy.onrender.com/screenshots/desktop.png)
![Mobile](https://qr-code-component-5kzy.onrender.com/screenshots/mobile.png)

### Links

- Solution URL: https://gitlab.com/frontend-mentor-challenge/qr-code-component
- Live Site URL: https://qr-code-component-5kzy.onrender.com

## My process

### Built with

- Semantic HTML5 markup
- CSS
- Flexbox
- Mobile-first workflow
- [Normalize](https://necolas.github.io/normalize.css/) - For CSS Reset

### What I learned


### Continued development


### Useful resources
- [Figma](https://www.figma.com/) - For design

## Author

- Website - [Issac Leyva](https://issacleyva.com) - In Construction
- Frontend Mentor - [@issleyva](https://www.frontendmentor.io/profile/issleyva)

## Acknowledgments